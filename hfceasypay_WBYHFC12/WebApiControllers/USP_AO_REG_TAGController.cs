﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using System.Data;
using System.Data.SqlClient;

using System.Web;
using System.Web.Configuration;

namespace hfceasypay_WBYHFC12.WebApiControllers
{

    //*********************************************************************
    //*********************************************************************
    //      ip
    //=====================================================================
    //http://localhost:59025/api/USP_AO_REG_TAG


    //*********************************************************************
    //*********************************************************************
    //      DTI(data to in)   and   DTO(data to out)   class
    //=====================================================================
    public class USP_AO_REG_TAG_DTI
    {
        public String SALES_NO { get; set; }
        public String MOBILE_NO { get; set; }
        public String MOBILE_ID { get; set; }
        public String TAG_NO { get; set; }
    }

    public class USP_AO_REG_TAG_DTO
    {
        public USP_AO_REG_TAG_DTO()
        {
            data = new Data();
        }

        public String ERROR_CODE { get; set; }
        public String ERROR_MSG { get; set; }
        public Data data;

        public class Data
        {
            public String LOGIN_RESULT { get; set; }
            public String ERR_MSG { get; set; }
            public String ERR_NO { get; set; }
            public String TAG_NO { get; set; }
        }
    }

    //*********************************************************************
    //*********************************************************************
    //      
    //=====================================================================
    public class USP_AO_REG_TAGController : ApiController
    {

        // GET api/<controller>/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<controller>
        public USP_AO_REG_TAG_DTO Post(USP_AO_REG_TAG_DTI model)
        {
            return USP_AO_REG_TAG(model);
        }

        private USP_AO_REG_TAG_DTO USP_AO_REG_TAG(USP_AO_REG_TAG_DTI model)
        {
            USP_AO_REG_TAG_DTO result = new USP_AO_REG_TAG_DTO();

            try
            {
                string connectionString = WebConfigurationManager.AppSettings["MSSQLConnection"].ToString();
                SqlConnection conn = new SqlConnection(connectionString);
                SqlCommand cmd = new SqlCommand(string.Empty, conn);
                conn.Open();

                cmd.Parameters.Clear();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "USP_AO_REG_TAG";

                cmd.Parameters.Add("@IN_SALES_NO", SqlDbType.VarChar).Value = model.SALES_NO;
                cmd.Parameters.Add("@IN_MOBILE_NO", SqlDbType.VarChar).Value = model.MOBILE_NO;
                cmd.Parameters.Add("@IN_MOBILE_ID", SqlDbType.VarChar).Value = model.MOBILE_ID;
                cmd.Parameters.Add("@IN_TAG_NO", SqlDbType.VarChar).Value = model.TAG_NO;


                SqlDataReader reader = cmd.ExecuteReader(); //CommandBehavior.SingleRow

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        result.data.LOGIN_RESULT = reader["LOGIN_RESULT"].ToString();
                        result.data.ERR_NO = reader["ERR_NO"].ToString();
                        result.data.ERR_MSG = reader["ERR_MSG"].ToString();
                        result.data.TAG_NO = reader["TAG_NO"].ToString();
                    }
                }

                reader.Close();
                reader.Dispose();

                conn.Close();
                conn.Dispose();
                //---------------------------
                //      Message
                //---------------------------
                result.ERROR_CODE = "200";
                result.ERROR_MSG = "成功";
            }
            catch (Exception ex)
            {
                result.ERROR_CODE = "001";
                result.ERROR_MSG = "發生例外: " + ex.ToString();
                //Console.WriteLine("Failed to connect to database due to" + ex.ToString());
            }

            return result;
        }

    }
}