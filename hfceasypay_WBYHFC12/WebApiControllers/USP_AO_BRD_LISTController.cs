﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Configuration;
using System.Web.Http;

namespace hfceasypay_WBYHFC12.WebApiControllers
{

    //*********************************************************************
    //*********************************************************************
    //      ip
    //=====================================================================
    //http://localhost:59025/api/USP_AO_BRD_LIST


    //*********************************************************************
    //*********************************************************************
    //      DTI(data to in)   and   DTO(data to out)   class
    //=====================================================================
    public class UspAoBrdListDTI
    {
        public String MOBILE_NO { get; set; }
        public String MOBILE_ID { get; set; }
    }




    public class UspAoBrdListDTO
    {
        public UspAoBrdListDTO()
        {
            data = new List<Data>();
        }

        public String ERROR_CODE { get; set; }
        public String ERROR_MSG { get; set; }

        public String ACCOUNT_STATUS_CODE;
        public String ACCOUNT_STATUS_MSG;

        public List<Data> data;

        public class Data
        {
            public String LOGIN_RESULT { get; set; }
            public String ERR_MSG { get; set; }
            public String ERR_NO { get; set; }
            public String BD_MSG_NO { get; set; }
            public String BD_MSG_TEXT { get; set; }

        }
    }





    //*********************************************************************
    //*********************************************************************
    //
    //=====================================================================
    public class USP_AO_BRD_LISTController : ApiController
    {


        // POST api/<controller>
        public UspAoBrdListDTO Post(UspAoBrdListDTI uspAoBrdListDTI)
        {
            ZzipBamsaleMlDTO zzipBamsaleMlDTO = new ZZIP_BAMSALE_ML().get(uspAoBrdListDTI.MOBILE_NO, uspAoBrdListDTI.MOBILE_ID);

            UspAoBrdListDTO uspAoBrdListDTO = new UspAoBrdListDTO();
            uspAoBrdListDTO = get_sp(uspAoBrdListDTI);

            if (zzipBamsaleMlDTO.RESULT_CODE != "000")  //  成功
            {
                uspAoBrdListDTO.ACCOUNT_STATUS_CODE = zzipBamsaleMlDTO.RESULT_CODE;
                uspAoBrdListDTO.ACCOUNT_STATUS_MSG = zzipBamsaleMlDTO.RESULT_MSG;
            }
            else
            {
                uspAoBrdListDTO.ACCOUNT_STATUS_CODE = "";
                uspAoBrdListDTO.ACCOUNT_STATUS_MSG = "";
            }
                return uspAoBrdListDTO;
        }


        //=================================================================
        //          get  USP_AO_BRD_LIST  store procedure
        //=================================================================
        private UspAoBrdListDTO get_sp(UspAoBrdListDTI uspAoBrdListDTI)
        {
            UspAoBrdListDTO uspAoBrdListDTO = new UspAoBrdListDTO();

            try
            {
                string connectionString = WebConfigurationManager.AppSettings["MSSQLConnection"].ToString();
                SqlConnection conn = new SqlConnection(connectionString);
                SqlCommand cmd = new SqlCommand(string.Empty, conn);
                conn.Open();

                cmd.Parameters.Clear();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "USP_AO_BRD_LIST";

                cmd.Parameters.Add("@IN_MOBILE_NO", SqlDbType.VarChar).Value = uspAoBrdListDTI.MOBILE_NO;
                cmd.Parameters.Add("@IN_MOBILE_ID", SqlDbType.VarChar).Value = uspAoBrdListDTI.MOBILE_ID;

                SqlDataReader reader = cmd.ExecuteReader(); //CommandBehavior.SingleRow

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        UspAoBrdListDTO.Data data = new UspAoBrdListDTO.Data();

                       data.LOGIN_RESULT = reader["LOGIN_RESULT"].ToString();
                       data.ERR_MSG = reader["ERR_MSG"].ToString();
                        data.ERR_NO = reader["ERR_NO"].ToString();
                        data.BD_MSG_NO = reader["BD_MSG_NO"].ToString();
                       data.BD_MSG_TEXT = reader["BD_MSG_TEXT"].ToString();

                        uspAoBrdListDTO.data.Add(data);
                    }
                }

                reader.Close();
                reader.Dispose();

                conn.Close();
                conn.Dispose();
                //---------------------------
                //      Message
                //---------------------------
                uspAoBrdListDTO.ERROR_CODE = "200";
                uspAoBrdListDTO.ERROR_MSG = "成功";

            }
            catch (Exception ex)
            {
                uspAoBrdListDTO.ERROR_CODE = "001";
                uspAoBrdListDTO.ERROR_MSG = "發生例外: " + ex.ToString();
                //Console.WriteLine("Failed to connect to database due to" + ex.ToString());
            }



            return uspAoBrdListDTO;
        }


    }
}


/*
         private UspAoBrdListDTO dummy (UspAoBrdListDTI uspAoBrdListDTI)
        {
            UspAoBrdListDTO uspAoBrdListDTO = new UspAoBrdListDTO();

            uspAoBrdListDTO.ERROR_CODE = "200";
            uspAoBrdListDTO.ERROR_MSG = "成功";

            uspAoBrdListDTO.data.LOGIN_RESULT = "dummyLOGIN_RESULT";
            uspAoBrdListDTO.data.ERR_MSG = "dummyERR_MSG";
            uspAoBrdListDTO.data.BD_MSG_NO = "dummyBD_MSG_NO";
            uspAoBrdListDTO.data.BD_MSG_TEXT = "dummyBD_MSG_TEXT";

            return uspAoBrdListDTO;
        }
 */
