﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Configuration;
using System.Web.Http;

namespace hfceasypay_WBYHFC12.WebApiControllers
{

    //*********************************************************************
    //*********************************************************************
    //      ip
    //=====================================================================
    //http://localhost:59025/api/USP_AO_APP_MSG


    //*********************************************************************
    //*********************************************************************
    //      DTI(data to in)   and   DTO(data to out)   class
    //=====================================================================
    public class USP_AO_APP_MSG_DTI
    {
        public String MOBILE_NO { get; set; }
        public String MOBILE_ID { get; set; }
        public String APP_CASE_NO { get; set; }

    }

    public class USP_AO_APP_MSG_DTO
    {
        public USP_AO_APP_MSG_DTO()
        {
            data = new List<Data>();
        }

        public String ERROR_CODE { get; set; }
        public String ERROR_MSG { get; set; }
        public List<Data> data;

        public class Data
        {
            public String LOGIN_RESULT { get; set; }
            public String ERR_MSG { get; set; }
            public String APP_CASE_NO { get; set; }
            public String MSG_NO { get; set; }
            public String MSG_CONTENT { get; set; }
            public String MSG_DATE { get; set; }
        }



    }



    //*********************************************************************
    //*********************************************************************
    //
    //=====================================================================
    public class USP_AO_APP_MSGController : ApiController
    {


        // POST api/<controller>
        public USP_AO_APP_MSG_DTO Post(USP_AO_APP_MSG_DTI usp_AO_APP_MSG_DTI)
        {
            return USP_AO_APP_MSG(usp_AO_APP_MSG_DTI);
        }

        private USP_AO_APP_MSG_DTO USP_AO_APP_MSG(USP_AO_APP_MSG_DTI usp_AO_APP_MSG_DTI)
        {
            USP_AO_APP_MSG_DTO usp_AO_APP_MSG_DTO = new USP_AO_APP_MSG_DTO();

            try
            {
                string connectionString = WebConfigurationManager.AppSettings["MSSQLConnection"].ToString();
                SqlConnection conn = new SqlConnection(connectionString);
                SqlCommand cmd = new SqlCommand(string.Empty, conn);
                conn.Open();

                cmd.Parameters.Clear();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "USP_AO_APP_MSG";

                cmd.Parameters.Add("@IN_MOBILE_NO", SqlDbType.VarChar).Value = usp_AO_APP_MSG_DTI.MOBILE_NO;
                cmd.Parameters.Add("@IN_MOBILE_ID", SqlDbType.VarChar).Value = usp_AO_APP_MSG_DTI.MOBILE_ID;
                cmd.Parameters.Add("@IN_APP_CASE_NO", SqlDbType.VarChar).Value = usp_AO_APP_MSG_DTI.APP_CASE_NO;

                SqlDataReader reader = cmd.ExecuteReader(); //CommandBehavior.SingleRow

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        USP_AO_APP_MSG_DTO.Data data = new USP_AO_APP_MSG_DTO.Data();
                        data.LOGIN_RESULT = reader["LOGIN_RESULT"].ToString();
                        data.ERR_MSG = reader["ERR_MSG"].ToString();
                        data.APP_CASE_NO = reader["APP_CASE_NO"].ToString();
                        data.MSG_NO = reader["MSG_NO"].ToString();
                        data.MSG_CONTENT = reader["MSG_CONTENT"].ToString();
                        data.MSG_DATE = reader["MSG_DATE"].ToString();
                        usp_AO_APP_MSG_DTO.data.Add(data);

                    }
                }

                reader.Close();
                reader.Dispose();

                conn.Close();
                conn.Dispose();
                //---------------------------
                //      Message
                //---------------------------
                usp_AO_APP_MSG_DTO.ERROR_CODE = ErrorInfo.OK_CODE;
                usp_AO_APP_MSG_DTO.ERROR_MSG = ErrorInfo.OK_MSG;

            }
            catch (Exception ex)
            {
                usp_AO_APP_MSG_DTO.ERROR_CODE = ErrorInfo.CATCH_CODE;
                usp_AO_APP_MSG_DTO.ERROR_MSG = ErrorInfo.CATCH_MSG + ex.ToString();
            }



            return usp_AO_APP_MSG_DTO;
        }
    }
}