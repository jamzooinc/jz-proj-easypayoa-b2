﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace hfceasypay_WBYHFC12.WebApiControllers
{


    public class ErrorInfo
    { 
        public static string OK_CODE = "200";
        public static string OK_MSG = "成功";


        public static string CATCH_CODE = "001";
        public static string CATCH_MSG = "發生例外: ";

        public static string IDNG_CODE = "002";
        public static string IDNG_MSG = "帳號錯誤";
    }
}