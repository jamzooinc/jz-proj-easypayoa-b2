﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Configuration;
using System.Web.Http;

namespace hfceasypay_WBYHFC12.WebApiControllers
{
    //*********************************************************************
    //*********************************************************************
    //      ip
    //=====================================================================
    //http://localhost:59025/api/USP_AO_PAY_LIST



    //*********************************************************************
    //*********************************************************************
    //      DTI(data to in)   and   DTO(data to out)   class
    //=====================================================================
    public class UspAoPayListDTI
    {
        public String SALES_NO { get; set; }
        public String MOBILE_NO { get; set; }
        public String MOBILE_ID { get; set; }
    }

    public class UspAoPayListDTO
    {
        public UspAoPayListDTO()
        {
            data = new List<Data>();
        }
        public String ERROR_CODE { get; set; }
        public String ERROR_MSG { get; set; }

        public String ACCOUNT_STATUS_CODE;
        public String ACCOUNT_STATUS_MSG;

        public List<Data> data;

        public class Data
        {
            public String LOGIN_RESULT { get; set; }
            public String ERR_MSG { get; set; }
            public String ERR_NO { get; set; }
            public String P_ID { get; set; }
            public String APPL_NO { get; set; }
            public String ENTER_DATE { get; set; }
            public String CH_NAME { get; set; }
            public String CASE_STATUS { get; set; }
            public String PROD_TYPE { get; set; }
            public String PROD_TP_NM { get; set; }
            public String BRAND_TYPE { get; set; }
            public String SUPPL_DATA { get; set; }
            public String MSG_CNT { get; set; }

            
        }
    }



    //*********************************************************************
    //*********************************************************************
    //
    //=====================================================================
    public class USP_AO_PAY_LISTController : ApiController
    {
        // POST api/<controller>
        public UspAoPayListDTO Post(UspAoPayListDTI uspAoPayListDTI)
        {
            ZzipBamsaleMlDTO zzipBamsaleMlDTO = new ZZIP_BAMSALE_ML().get(uspAoPayListDTI.MOBILE_NO, uspAoPayListDTI.MOBILE_ID);

            UspAoPayListDTO uspAoPayListDTO = new UspAoPayListDTO();
            uspAoPayListDTO = get_sp(uspAoPayListDTI);

            if (zzipBamsaleMlDTO.RESULT_CODE != "000")  //  成功
            {
                uspAoPayListDTO.ACCOUNT_STATUS_CODE = zzipBamsaleMlDTO.RESULT_CODE;
                uspAoPayListDTO.ACCOUNT_STATUS_MSG = zzipBamsaleMlDTO.RESULT_MSG;
            }
            else
            {
                uspAoPayListDTO.ACCOUNT_STATUS_CODE = "";
                uspAoPayListDTO.ACCOUNT_STATUS_MSG = "";
            }

            return uspAoPayListDTO;
        }


        //=================================================================
        //          get USP_AO_PAY_LIST stored procedure
        //=================================================================
        private UspAoPayListDTO get_sp(UspAoPayListDTI uspAoAppUpdateDTI)
        {
            UspAoPayListDTO uspAoPayListDTO = new UspAoPayListDTO();

            try
            {
                string connectionString = WebConfigurationManager.AppSettings["MSSQLConnection"].ToString();
                SqlConnection conn = new SqlConnection(connectionString);
                SqlCommand cmd = new SqlCommand(string.Empty, conn);
                conn.Open();

                cmd.Parameters.Clear();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "USP_AO_PAY_LIST";

                cmd.Parameters.Add("@IN_SALES_NO", SqlDbType.VarChar).Value = uspAoAppUpdateDTI.SALES_NO;
                cmd.Parameters.Add("@IN_MOBILE_NO", SqlDbType.VarChar).Value = uspAoAppUpdateDTI.MOBILE_NO;
                cmd.Parameters.Add("@IN_MOBILE_ID", SqlDbType.VarChar).Value = uspAoAppUpdateDTI.MOBILE_ID;


                SqlDataReader reader = cmd.ExecuteReader(); //CommandBehavior.SingleRow

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        UspAoPayListDTO.Data data = new UspAoPayListDTO.Data();

                        data.LOGIN_RESULT = reader["LOGIN_RESULT"].ToString();
                        data.ERR_MSG = reader["ERR_MSG"].ToString();
                        data.ERR_NO = reader["ERR_NO"].ToString();
                        data.P_ID = reader["P_ID"].ToString();
                        data.APPL_NO = reader["APPL_NO"].ToString();
                        data.ENTER_DATE = reader["ENTER_DATE"].ToString();
                        data.CH_NAME = reader["CH_NAME"].ToString();
                        data.CASE_STATUS = reader["CASE_STATUS"].ToString();
                        data.PROD_TYPE = reader["PROD_TYPE"].ToString();
                        data.PROD_TP_NM = reader["PROD_TP_NM"].ToString();
                        data.BRAND_TYPE = reader["BRAND_TYPE"].ToString();
                        data.SUPPL_DATA = reader["SUPPL_DATA"].ToString();
                        data.MSG_CNT = reader["MSG_CNT"].ToString();

                        uspAoPayListDTO.data.Add(data);
                    }
                }

                reader.Close();
                reader.Dispose();

                conn.Close();
                conn.Dispose();
                //---------------------------
                //      Message
                //---------------------------
                uspAoPayListDTO.ERROR_CODE = "200";
                uspAoPayListDTO.ERROR_MSG = "成功";

            }
            catch (Exception ex)
            {
                uspAoPayListDTO.ERROR_CODE = "001";
                uspAoPayListDTO.ERROR_MSG = "發生例外: " + ex.ToString();
                //Console.WriteLine("Failed to connect to database due to" + ex.ToString());
            }



            return uspAoPayListDTO;
        }

    }
}