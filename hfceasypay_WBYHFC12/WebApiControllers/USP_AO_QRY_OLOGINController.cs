﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Configuration;
using System.Web.Http;

namespace hfceasypay_WBYHFC12.WebApiControllers
{
    //*********************************************************************
    //*********************************************************************
    //      ip
    //=====================================================================
    //http://localhost:59025/api/USP_AO_QRY_OLOGIN

    //*********************************************************************
    //*********************************************************************
    //
    //=====================================================================
    public class USP_AO_QRY_OLOGINController : ApiController
    {
        //*********************************************************************
        //*********************************************************************
        //      DTI(data to in)   and   DTO(data to out)   class
        //=====================================================================
        public class InputModel
        {
            public String SALES_NO { get; set; }
            public String MOBILE_NO { get; set; }
            public String MOBILE_ID { get; set; }
        }

        public class OutputModel
        {
            public OutputModel()
            {
                Data = new OutputDataModel();
            }

            public String ERROR_CODE { get; set; }
            public String ERROR_MSG { get; set; }

            public OutputDataModel Data;
        }

        public class OutputDataModel
        {
            public String EMPL_ID { get; set; }
            public String PASWD { get; set; }
            public String USER_LOGIN_ID { get; set; }
        }

        public class LoginDTO
        {
            public LoginDTO()
            {
                data = new Data();
            }

            public String ERROR_CODE { get; set; }
            public String ERROR_MSG { get; set; }
            public Data data;

            public class Data
            {
                public String LOGIN_RESULT { get; set; }
                public String ERR_NO { get; set; }
                public String ERR_MSG { get; set; }
                public String EMPL_ID { get; set; }
                public String PASWD { get; set; }
                public String USER_LOGIN_ID { get; set; }
                
            }
        }


        // POST api/<controller>
        public LoginDTO Post(InputModel loginDTI)
        {
            return get(loginDTI);
        }


        private LoginDTO get(InputModel loginDTI)
        {
            LoginDTO loginDTO = new LoginDTO();

            try
            {
                string connectionString = WebConfigurationManager.AppSettings["MSSQLConnection"].ToString();
                SqlConnection conn = new SqlConnection(connectionString);
                SqlCommand cmd = new SqlCommand(string.Empty, conn);
                conn.Open();

                cmd.Parameters.Clear();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "USP_AO_QRY_OLOGIN";

                cmd.Parameters.Add("@IN_SALES_NO", SqlDbType.VarChar).Value = loginDTI.SALES_NO;           //"????";
                cmd.Parameters.Add("@IN_MOBILE_NO", SqlDbType.VarChar).Value = loginDTI.MOBILE_NO;         //"0937121235";
                cmd.Parameters.Add("@IN_MOBILE_ID", SqlDbType.VarChar).Value = loginDTI.MOBILE_ID;         //"edfvghyt123";

                SqlDataReader reader = cmd.ExecuteReader(); //CommandBehavior.SingleRow

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        loginDTO.data.LOGIN_RESULT = reader["LOGIN_RESULT"].ToString();
                        loginDTO.data.ERR_NO = reader["ERR_NO"].ToString();
                        loginDTO.data.ERR_MSG = reader["ERR_MSG"].ToString();
                        loginDTO.data.EMPL_ID = reader["EMPL_ID"].ToString();
                        loginDTO.data.PASWD = reader["PASWD"].ToString();
                        loginDTO.data.USER_LOGIN_ID = reader["USER_LOGIN_ID"].ToString();
                    }
                }

                reader.Close();
                reader.Dispose();

                conn.Close();
                conn.Dispose();
                //---------------------------
                //      Message
                //---------------------------
                loginDTO.ERROR_CODE = "200";
                loginDTO.ERROR_MSG = "成功";

            }
            catch (Exception ex)
            {
                loginDTO.ERROR_CODE = "001";
                loginDTO.ERROR_MSG = "發生例外: " + ex.ToString();
                //Console.WriteLine("Failed to connect to database due to" + ex.ToString());
            }



            return loginDTO;
        }

    }
}
