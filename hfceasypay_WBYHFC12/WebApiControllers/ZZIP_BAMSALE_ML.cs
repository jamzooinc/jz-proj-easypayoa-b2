﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Configuration;
using System.Web.Http;

namespace hfceasypay_WBYHFC12.WebApiControllers
{
    //*********************************************************************
    //*********************************************************************
    //
    //      這個 function 是檢查 帳號是否正確與存在
    //
    //=====================================================================

    //*********************************************************************
    //*********************************************************************
    //      DTI(data to in)   and   DTO(data to out)   class
    //=====================================================================

    public class ZzipBamsaleMlDTO
    {
        public ZzipBamsaleMlDTO()
        {
            data = new Data();
        }
        public String RESULT_CODE { get; set; }
        public String RESULT_MSG { get; set; }
        public Data data;

        public class Data
        {
            public String MOBILE_IDN { get; set; }
            public String MOBILE_NO { get; set; }
        }

    }




    //*********************************************************************
    //*********************************************************************
    //
    //=====================================================================
    public class ZZIP_BAMSALE_ML
    {

        //---------------------------------------------
        //      get  ZZIP_BAMSALE_ML  TABLE
        //---------------------------------------------
        public ZzipBamsaleMlDTO get(string MOBILE_NO, string MOBILE_ID)
        {
            ZzipBamsaleMlDTO zzipBamsaleMlDTO = new ZzipBamsaleMlDTO();

            try
            {
                string connectionString = WebConfigurationManager.AppSettings["MSSQLConnection"].ToString();

                SqlConnection ConnMSSQL = new SqlConnection(connectionString);
                ConnMSSQL.Open();

                string[] parameterValue = new string[1];
                parameterValue[0] = MOBILE_NO;
                String sqlStr = @" SELECT * FROM ZZIP_BAMSALE_ML where MOBILE_NO in({0})";

                string[] parameters = parameterValue.Select((s, i) => "@NAME" + i.ToString()).ToArray();
                sqlStr = string.Format(sqlStr, string.Join(",", parameters));

                SqlCommand sqlCmd = new SqlCommand(sqlStr, ConnMSSQL);
                for (int i = 0; i < parameters.Length; i++)
                    sqlCmd.Parameters.AddWithValue(parameters[i], parameterValue[i]);

                SqlDataReader dr = sqlCmd.ExecuteReader();

                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        zzipBamsaleMlDTO.data.MOBILE_NO = dr["MOBILE_NO"].ToString();
                        zzipBamsaleMlDTO.data.MOBILE_IDN = dr["MOBILE_IDN"].ToString();
                    }

                    //----  檢查 MOBILE_NO  與  MOBILE_IDN
                    if (zzipBamsaleMlDTO.data.MOBILE_IDN == "")
                    {
                        zzipBamsaleMlDTO.RESULT_CODE = "200";
                        zzipBamsaleMlDTO.RESULT_MSG = "識別碼已為空(重複清空不用再清除識別碼)";
                    }
                    else if ((zzipBamsaleMlDTO.data.MOBILE_NO != MOBILE_NO) || (zzipBamsaleMlDTO.data.MOBILE_IDN != MOBILE_ID))
                    {
                        zzipBamsaleMlDTO.RESULT_CODE = "100";
                        zzipBamsaleMlDTO.RESULT_MSG = " 查無資料(查無此比手機號碼或識別碼)";
                    }
                    else
                    {
                        zzipBamsaleMlDTO.RESULT_CODE = "000";
                        zzipBamsaleMlDTO.RESULT_MSG = "成功";
                    }

                }
                else
                {
                    //---  查無 此手機號碼
                    zzipBamsaleMlDTO.RESULT_CODE = "100";
                    zzipBamsaleMlDTO.RESULT_MSG = " 查無資料(查無此比手機號碼或識別碼)";
                }

                //------------------
                //  關閉資料庫連線
                //------------------
                ConnMSSQL.Close();
                ConnMSSQL.Dispose();

            }
            catch (Exception ex)
            {
                zzipBamsaleMlDTO.RESULT_CODE = ErrorInfo.CATCH_CODE;
                zzipBamsaleMlDTO.RESULT_MSG = ErrorInfo.CATCH_MSG + ex.ToString();
            }



            return zzipBamsaleMlDTO;

        }


    }
}