﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Configuration;
using System.Web.Http;

namespace hfceasypay_WBYHFC12.WebApiControllers
{

    //*********************************************************************
    //*********************************************************************
    //      ip
    //=====================================================================
    //http://localhost:59025/api/USP_AO_SIGIN_DO


    //*********************************************************************
    //*********************************************************************
    //      DTI(data to in)   and   DTO(data to out)   class
    //=====================================================================
    public class USP_AO_SIGIN_DO_DTI
    {
        public String MOBILE_NO { get; set; }
        public String MOBILE_ID { get; set; }
        public String appCaseNo { get; set; }
        public String applNo { get; set; }
        public String siginTitle { get; set; }
        public String siginMemo { get; set; }

    }

    public class USP_AO_SIGIN_DO_DTO
    {
        public USP_AO_SIGIN_DO_DTO()
        {
            data = new List<Data>();
        }

        public String ERROR_CODE { get; set; }
        public String ERROR_MSG { get; set; }
        public List<Data> data;

        public class Data
        {
            public String LOGIN_RESULT { get; set; }
            
        }
    }



    //*********************************************************************
    //*********************************************************************
    //
    //=====================================================================
    public class USP_AO_SIGIN_DOController : ApiController
    {


        // POST api/<controller>
        public USP_AO_SIGIN_DO_DTO Post(USP_AO_SIGIN_DO_DTI USP_AO_SIGIN_DO_DTI)
        {
            return USP_AO_SIGIN_DO(USP_AO_SIGIN_DO_DTI);
        }

        private USP_AO_SIGIN_DO_DTO USP_AO_SIGIN_DO(USP_AO_SIGIN_DO_DTI USP_AO_SIGIN_DO_DTI)
        {
            USP_AO_SIGIN_DO_DTO USP_AO_SIGIN_DO_DTO = new USP_AO_SIGIN_DO_DTO();

            try
            {
                string connectionString = WebConfigurationManager.AppSettings["MSSQLConnection"].ToString();
                SqlConnection conn = new SqlConnection(connectionString);
                SqlCommand cmd = new SqlCommand(string.Empty, conn);
                conn.Open();

                cmd.Parameters.Clear();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "USP_AO_SIGIN_DO";

                cmd.Parameters.Add("@IN_MOBILE_NO", SqlDbType.VarChar).Value = USP_AO_SIGIN_DO_DTI.MOBILE_NO;
                cmd.Parameters.Add("@IN_MOBILE_ID", SqlDbType.VarChar).Value = USP_AO_SIGIN_DO_DTI.MOBILE_ID;
                cmd.Parameters.Add("@IN_APP_CASE_NO", SqlDbType.VarChar).Value = USP_AO_SIGIN_DO_DTI.appCaseNo;
                cmd.Parameters.Add("@IN_APPL_NO", SqlDbType.VarChar).Value = USP_AO_SIGIN_DO_DTI.applNo;
                cmd.Parameters.Add("@IN_SIGIN_TITLE", SqlDbType.VarChar).Value = USP_AO_SIGIN_DO_DTI.siginTitle;
                cmd.Parameters.Add("@IN_SIGIN_MEMO", SqlDbType.VarChar).Value = USP_AO_SIGIN_DO_DTI.siginMemo;

                SqlDataReader reader = cmd.ExecuteReader(); //CommandBehavior.SingleRow

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        USP_AO_SIGIN_DO_DTO.Data data = new USP_AO_SIGIN_DO_DTO.Data();
                        data.LOGIN_RESULT = reader["LOGIN_RESULT"].ToString();
                        USP_AO_SIGIN_DO_DTO.ERROR_CODE = reader["ERR_NO"].ToString();
                        USP_AO_SIGIN_DO_DTO.ERROR_MSG = reader["ERR_MSG"].ToString();

                        USP_AO_SIGIN_DO_DTO.data.Add(data);

                    }
                }

                reader.Close();
                reader.Dispose();

                conn.Close();
                conn.Dispose();
                //---------------------------
                //      Message
                //---------------------------
                if (string.IsNullOrEmpty(USP_AO_SIGIN_DO_DTO.ERROR_CODE))
                {
                    USP_AO_SIGIN_DO_DTO.ERROR_CODE = ErrorInfo.OK_CODE;
                    USP_AO_SIGIN_DO_DTO.ERROR_MSG = ErrorInfo.OK_MSG;
                }
                else
                {
                    if (USP_AO_SIGIN_DO_DTO.ERROR_CODE == "200")
                        USP_AO_SIGIN_DO_DTO.ERROR_CODE = "301";
                }

            }
            catch (Exception ex)
            {
                USP_AO_SIGIN_DO_DTO.ERROR_CODE = ErrorInfo.CATCH_CODE;
                USP_AO_SIGIN_DO_DTO.ERROR_MSG = ErrorInfo.CATCH_MSG + ex.ToString();
            }



            return USP_AO_SIGIN_DO_DTO;
        }
    }
}