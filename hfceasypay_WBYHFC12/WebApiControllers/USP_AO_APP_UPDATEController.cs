﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Configuration;
using System.Web.Http;

namespace hfceasypay_WBYHFC12.WebApiControllers
{

    //*********************************************************************
    //*********************************************************************
    //      ip
    //=====================================================================
    //http://localhost:59025/api/USP_AO_APP_UPDATE


    //*********************************************************************
    //*********************************************************************
    //      DTI(data to in)   and   DTO(data to out)   class
    //=====================================================================
    public class UspAoAppUpdateDTI
    {
        public String MOBILE_NO { get; set; }
        public String MOBILE_ID { get; set; }
        public String APP_CASE_NO { get; set; }
    }

    public class UspAoAppUpdateDTO
    {
        public UspAoAppUpdateDTO()
        {
            data = new Data();
        }

        public String ERROR_CODE { get; set; }
        public String ERROR_MSG { get; set; }
        public Data data;

        public class Data
        {
            public String LOGIN_RESULT { get; set; }
            public String ERR_MSG { get; set; }
            public String ERR_NO { get; set; }
            public String APP_CASE_NO { get; set; }
            public String APPL_NO { get; set; }
            public String P_ID { get; set; }
            public String CH_NAME { get; set; }
            public String CASE_STATUS { get; set; }
            public String UPDATE_FLG { get; set; }
            public String APP_FORM_2 { get; set; }
            public String PROD_TYPE { get; set; }
            public String PROD_TYPE_NM { get; set; }
            public String FUND_SRC { get; set; }
            public String FUND_SRC_NM { get; set; }
            public String BANK_NA { get; set; }
            public String BANK_NA_NM { get; set; }
            public String NOQRY_FLG { get; set; }
            public String PROJ_ID { get; set; }
            public String PROJ_ID_NM { get; set; }
            public String DLR_NO { get; set; }
            public String DLR_NO_NM { get; set; }
            public String BRNH_NO { get; set; }
            public String BRNH_NO_NM { get; set; }
            public String SALES_NO { get; set; }
            public String SALES_NO_NM { get; set; }
            public String MSG_CNT { get; set; }
            public String SEQ_NO { get; set; }
            public String BACK_MENT { get; set; }
        }
    }



    //*********************************************************************
    //*********************************************************************
    //
    //=====================================================================
    public class USP_AO_APP_UPDATEController : ApiController
    {


        // POST api/<controller>
        public UspAoAppUpdateDTO Post(UspAoAppUpdateDTI uspAoAppUpdateDTI)
        {
            return get(uspAoAppUpdateDTI);
        }


        private UspAoAppUpdateDTO get(UspAoAppUpdateDTI uspAoAppUpdateDTI)
        {
            UspAoAppUpdateDTO uspAoAppUpdateDTO = new UspAoAppUpdateDTO();

            try
            {
                string connectionString = WebConfigurationManager.AppSettings["MSSQLConnection"].ToString();
                SqlConnection conn = new SqlConnection(connectionString);
                SqlCommand cmd = new SqlCommand(string.Empty, conn);
                conn.Open();

                cmd.Parameters.Clear();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "USP_AO_APP_UPDATE";

                cmd.Parameters.Add("@IN_MOBILE_NO", SqlDbType.VarChar).Value = uspAoAppUpdateDTI.MOBILE_NO; 
                cmd.Parameters.Add("@IN_MOBILE_ID", SqlDbType.VarChar).Value = uspAoAppUpdateDTI.MOBILE_ID; 
                cmd.Parameters.Add("@IN_APP_CASE_NO_STR", SqlDbType.VarChar).Value = uspAoAppUpdateDTI.APP_CASE_NO; 

                SqlDataReader reader = cmd.ExecuteReader(); //CommandBehavior.SingleRow

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        uspAoAppUpdateDTO.data.LOGIN_RESULT = reader["LOGIN_RESULT"].ToString();
                        uspAoAppUpdateDTO.data.ERR_MSG = reader["ERR_MSG"].ToString();
                        uspAoAppUpdateDTO.data.ERR_NO = reader["ERR_NO"].ToString();
                        uspAoAppUpdateDTO.data.APP_CASE_NO = reader["APP_CASE_NO"].ToString();
                        uspAoAppUpdateDTO.data.APPL_NO = reader["APPL_NO"].ToString();
                        uspAoAppUpdateDTO.data.P_ID = reader["P_ID"].ToString();
                        uspAoAppUpdateDTO.data.CH_NAME = reader["CH_NAME"].ToString();
                        uspAoAppUpdateDTO.data.CASE_STATUS = reader["CASE_STATUS"].ToString();
                        uspAoAppUpdateDTO.data.UPDATE_FLG = reader["UPDATE_FLG"].ToString();
                        uspAoAppUpdateDTO.data.PROD_TYPE = reader["PROD_TYPE"].ToString();
                        uspAoAppUpdateDTO.data.PROD_TYPE_NM = reader["PROD_TYPE_NM"].ToString();
                        uspAoAppUpdateDTO.data.FUND_SRC = reader["FUND_SRC"].ToString();
                        uspAoAppUpdateDTO.data.FUND_SRC_NM = reader["FUND_SRC_NM"].ToString();
                        uspAoAppUpdateDTO.data.BANK_NA = reader["BANK_NA"].ToString();
                        uspAoAppUpdateDTO.data.BANK_NA_NM = reader["BANK_NA_NM"].ToString();

                        uspAoAppUpdateDTO.data.PROJ_ID = reader["PROJ_ID"].ToString();
                        uspAoAppUpdateDTO.data.PROJ_ID_NM = reader["PROJ_ID_NM"].ToString();
                        uspAoAppUpdateDTO.data.DLR_NO = reader["DLR_NO"].ToString();
                        uspAoAppUpdateDTO.data.DLR_NO_NM = reader["DLR_NO_NM"].ToString();
                        uspAoAppUpdateDTO.data.BRNH_NO = reader["BRNH_NO"].ToString();
                        uspAoAppUpdateDTO.data.BRNH_NO_NM = reader["BRNH_NO_NM"].ToString();
                        uspAoAppUpdateDTO.data.SALES_NO = reader["SALES_NO"].ToString();
                        uspAoAppUpdateDTO.data.SALES_NO_NM = reader["SALES_NO_NM"].ToString();
                        uspAoAppUpdateDTO.data.MSG_CNT = reader["MSG_CNT"].ToString();
                        uspAoAppUpdateDTO.data.SEQ_NO = reader["SEQ_NO"].ToString();
                        uspAoAppUpdateDTO.data.BACK_MENT = reader["BACK_MENT"].ToString();
                    }
                }

                reader.Close();
                reader.Dispose();

                conn.Close();
                conn.Dispose();
                //---------------------------
                //      Message
                //---------------------------
                uspAoAppUpdateDTO.ERROR_CODE = "200";
                uspAoAppUpdateDTO.ERROR_MSG = "成功";

            }
            catch (Exception ex)
            {
                uspAoAppUpdateDTO.ERROR_CODE = "001";
                uspAoAppUpdateDTO.ERROR_MSG = "發生例外: " + ex.ToString();
                //Console.WriteLine("Failed to connect to database due to" + ex.ToString());
            }



            return uspAoAppUpdateDTO;
        }

    }
}