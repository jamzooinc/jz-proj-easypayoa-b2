﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using System.Data;
using System.Data.SqlClient;

using System.Web;
using System.Web.Configuration;

namespace hfceasypay_WBYHFC12.WebApiControllers
{

    //*********************************************************************
    //*********************************************************************
    //      ip
    //=====================================================================
    //http://localhost:59025/api/USP_AO_AGENT_UPD


    //*********************************************************************
    //*********************************************************************
    //      DTI(data to in)   and   DTO(data to out)   class
    //=====================================================================
    public class USP_AO_AGENT_UPD_DTI
    {
        public String SALES_NO { get; set; }
        public String MOBILE_NO { get; set; }
        //public String MOBILE_ID { get; set; }
    }

    public class USP_AO_AGENT_UPD_DTO
    {
        public USP_AO_AGENT_UPD_DTO()
        {
            data = new Data();
        }

        public String ERROR_CODE { get; set; }
        public String ERROR_MSG { get; set; }
        public Data data;

        public class Data
        {
            public String LOGIN_RESULT { get; set; }
            public String ERR_MSG { get; set; }
            public String ERR_NO { get; set; }
        }


    }

    //*********************************************************************
    //*********************************************************************
    //      
    //=====================================================================
    public class USP_AO_AGENT_UPDController : ApiController
    {

        // GET api/<controller>/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<controller>
        public USP_AO_AGENT_UPD_DTO Post(USP_AO_AGENT_UPD_DTI usp_AO_AGENT_UPD_DTI)
        {
            return USP_AO_AGENT_UPD(usp_AO_AGENT_UPD_DTI);
        }

        private USP_AO_AGENT_UPD_DTO USP_AO_AGENT_UPD(USP_AO_AGENT_UPD_DTI usp_AO_AGENT_UPD_DTI)
        {
            USP_AO_AGENT_UPD_DTO usp_AO_AGENT_UPD_DTO = new USP_AO_AGENT_UPD_DTO();

            try
            {
                string connectionString = WebConfigurationManager.AppSettings["MSSQLConnection"].ToString();
                SqlConnection conn = new SqlConnection(connectionString);
                SqlCommand cmd = new SqlCommand(string.Empty, conn);
                conn.Open();

                cmd.Parameters.Clear();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "USP_AO_AGENT_UPD";

                cmd.Parameters.Add("@IN_SALES_NO", SqlDbType.VarChar).Value = usp_AO_AGENT_UPD_DTI.SALES_NO;
                cmd.Parameters.Add("@IN_MOBILE_NO", SqlDbType.VarChar).Value = usp_AO_AGENT_UPD_DTI.MOBILE_NO;
                //cmd.Parameters.Add("@IN_MOBILE_ID", SqlDbType.VarChar).Value = usp_AO_AGENT_UPD_DTI.MOBILE_ID;


                SqlDataReader reader = cmd.ExecuteReader(); //CommandBehavior.SingleRow

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        usp_AO_AGENT_UPD_DTO.data.LOGIN_RESULT = reader["LOGIN_RESULT"].ToString();
                        usp_AO_AGENT_UPD_DTO.data.ERR_NO = reader["ERR_NO"].ToString();
                        usp_AO_AGENT_UPD_DTO.data.ERR_MSG = reader["ERR_MSG"].ToString();
                    }
                }

                reader.Close();
                reader.Dispose();

                conn.Close();
                conn.Dispose();
                //---------------------------
                //      Message
                //---------------------------
                usp_AO_AGENT_UPD_DTO.ERROR_CODE = "200";
                usp_AO_AGENT_UPD_DTO.ERROR_MSG = "成功";
            }
            catch (Exception ex)
            {
                usp_AO_AGENT_UPD_DTO.ERROR_CODE = "001";
                usp_AO_AGENT_UPD_DTO.ERROR_MSG = "發生例外: " + ex.ToString();
                //Console.WriteLine("Failed to connect to database due to" + ex.ToString());
            }

            return usp_AO_AGENT_UPD_DTO;
        }

    }
}