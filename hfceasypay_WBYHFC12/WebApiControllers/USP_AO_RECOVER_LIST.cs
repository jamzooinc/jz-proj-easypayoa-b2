﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Configuration;
using System.Web.Http;

namespace hfceasypay_WBYHFC12.WebApiControllers
{


    //*********************************************************************
    //*********************************************************************
    //      ip
    //=====================================================================
    //http://localhost:59025/api/USP_AO_RECOVER_LIST


    //*********************************************************************
    //*********************************************************************
    //      DTI(data to in)   and   DTO(data to out)   class
    //=====================================================================
    public class USP_AO_RECOVER_LIST_DTI
    {
        public string SALES_NO { get; set; }
        public String MOBILE_NO { get; set; }
        public String MOBILE_ID { get; set; }
    }

    public class USP_AO_RECOVER_LIST_DTO
    {
        public USP_AO_RECOVER_LIST_DTO()
        {
            data = new List<Data>();
        }

        public String ERROR_CODE { get; set; }
        public String ERROR_MSG { get; set; }

        public List<Data> data;

        public class Data
        {
            public string LOGIN_RESULT { get; set; }
            public string ERR_MSG { get; set; }
            public string ERR_NO { get; set; }
            public string ENTER_DATE { get; set; }
            public string APP_CASE_NO { get; set; }
            public string CH_NAME { get; set; }
            public string P_ID { get; set; }
            public string COM_FLG { get; set; }
            public string GUARD1_ID { get; set; }
            public string GUARD1_NAME { get; set; }
            public string GUARD2_ID { get; set; }
            public string GUARD2_NAME { get; set; }
            public string GUARD3_ID { get; set; }
            public string GUARD3_NAME { get; set; }
            public string GUARD4_ID { get; set; }
            public string GUARD4_NAME { get; set; }
            public string GUARD5_ID { get; set; }
            public string GUARD5_NAME { get; set; }
        }
    }
    
    //*********************************************************************
    //*********************************************************************
    //
    //=====================================================================

    public class USP_AO_RECOVER_LISTController : ApiController
    {
        // POST api/<controller>
        public USP_AO_RECOVER_LIST_DTO Post(USP_AO_RECOVER_LIST_DTI USP_AO_RECOVER_LIST_DTI)
        {
            return USP_AO_RECOVER_LIST(USP_AO_RECOVER_LIST_DTI);
        }

        private USP_AO_RECOVER_LIST_DTO USP_AO_RECOVER_LIST(USP_AO_RECOVER_LIST_DTI USP_AO_RECOVER_LIST_DTI)
        {
            USP_AO_RECOVER_LIST_DTO USP_AO_RECOVER_LIST_DTO = new USP_AO_RECOVER_LIST_DTO();


            try
            {
                string connectionString = WebConfigurationManager.AppSettings["MSSQLConnection"].ToString();
                SqlConnection conn = new SqlConnection(connectionString);
                SqlCommand cmd = new SqlCommand(string.Empty, conn);
                conn.Open();

                cmd.Parameters.Clear();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "USP_AO_RECOVER_LIST";

                cmd.Parameters.Add("@IN_MOBILE_NO", SqlDbType.VarChar).Value = USP_AO_RECOVER_LIST_DTI.MOBILE_NO;
                cmd.Parameters.Add("@IN_MOBILE_ID", SqlDbType.VarChar).Value = USP_AO_RECOVER_LIST_DTI.MOBILE_ID;
                cmd.Parameters.Add("@IN_SALES_NO", SqlDbType.VarChar).Value = USP_AO_RECOVER_LIST_DTI.SALES_NO;

                SqlDataReader reader = cmd.ExecuteReader(); //CommandBehavior.SingleRow

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        USP_AO_RECOVER_LIST_DTO.Data d = new USP_AO_RECOVER_LIST_DTO.Data();
                        d.ERR_MSG = reader["ERR_MSG"].ToString();
                        d.ERR_NO = reader["ERR_NO"].ToString();
                        d.LOGIN_RESULT = reader["LOGIN_RESULT"].ToString();

                        d.APP_CASE_NO = reader["APP_CASE_NO"].ToString();
                        d.ENTER_DATE = reader["ENTER_DATE"].ToString();
                        d.CH_NAME = reader["CH_NAME"].ToString();
                        d.COM_FLG = reader["COM_FLG"].ToString();
                        d.P_ID = reader["P_ID"].ToString();
                        d.GUARD1_ID = reader["GUARD1_ID"].ToString();
                        d.GUARD1_NAME = reader["GUARD1_NAME"].ToString();
                        d.GUARD2_ID = reader["GUARD2_ID"].ToString();
                        d.GUARD2_NAME = reader["GUARD2_NAME"].ToString();
                        d.GUARD3_ID = reader["GUARD3_ID"].ToString();
                        d.GUARD3_NAME = reader["GUARD3_NAME"].ToString();
                        d.GUARD4_ID = reader["GUARD4_ID"].ToString();
                        d.GUARD4_NAME = reader["GUARD4_NAME"].ToString();
                        d.GUARD5_ID = reader["GUARD5_ID"].ToString();
                        d.GUARD5_NAME = reader["GUARD5_NAME"].ToString();
                        USP_AO_RECOVER_LIST_DTO.data.Add(d);
                    }
                }

                reader.Close();
                reader.Dispose();

                conn.Close();
                conn.Dispose();
                //---------------------------
                //      Message
                //---------------------------
                USP_AO_RECOVER_LIST_DTO.ERROR_CODE = ErrorInfo.OK_CODE;
                USP_AO_RECOVER_LIST_DTO.ERROR_MSG = ErrorInfo.OK_MSG;

            }
            catch (Exception ex)
            {
                USP_AO_RECOVER_LIST_DTO.ERROR_CODE = ErrorInfo.CATCH_CODE;
                USP_AO_RECOVER_LIST_DTO.ERROR_MSG = ErrorInfo.CATCH_MSG + ex.ToString();
            }



            return USP_AO_RECOVER_LIST_DTO;
        }

    }
}