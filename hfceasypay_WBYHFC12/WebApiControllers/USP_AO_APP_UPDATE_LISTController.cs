﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Configuration;
using System.Web.Http;

namespace hfceasypay_WBYHFC12.WebApiControllers
{


    //*********************************************************************
    //*********************************************************************
    //
    //=====================================================================
    public class USP_AO_APP_UPDATE_LISTController : ApiController
    {
        //*********************************************************************
        //*********************************************************************
        //      ip
        //=====================================================================
        //http://localhost:59025/api/USP_AO_APP_UPDATE


        //*********************************************************************
        //*********************************************************************
        //      DTI(data to in)   and   DTO(data to out)   class
        //=====================================================================
        public class UspAoAppUpdateDTI
        {
            public String MOBILE_NO { get; set; }
            public String MOBILE_ID { get; set; }
            public String APP_CASE_NO { get; set; }
        }

        public class UspAoAppUpdateDTO
        {
            public UspAoAppUpdateDTO()
            {
                data = new List<Data>();
            }

            public String ERROR_CODE { get; set; }
            public String ERROR_MSG { get; set; }
            public List<Data> data;

            public class Data
            {
                public String LOGIN_RESULT { get; set; }
                public String ERR_MSG { get; set; }
                public String ERR_NO { get; set; }
                public String APP_CASE_NO { get; set; }
                public String APPL_NO { get; set; }
                public String P_ID { get; set; }
                public String CH_NAME { get; set; }
                public String CASE_STATUS { get; set; }
                public String UPDATE_FLG { get; set; }
                public String APP_FORM_2 { get; set; }
                public String PROD_TYPE { get; set; }
                public String PROD_TYPE_NM { get; set; }
                public String FUND_SRC { get; set; }
                public String FUND_SRC_NM { get; set; }
                public String BANK_NA { get; set; }
                public String BANK_NA_NM { get; set; }
                public String NOQRY_FLG { get; set; }
                public String PROJ_ID { get; set; }
                public String PROJ_ID_NM { get; set; }
                public String DLR_NO { get; set; }
                public String DLR_NO_NM { get; set; }
                public String BRNH_NO { get; set; }
                public String BRNH_NO_NM { get; set; }
                public String SALES_NO { get; set; }
                public String SALES_NO_NM { get; set; }
                public String MSG_CNT { get; set; }
                public String SEQ_NO { get; set; }
                public String BACK_MENT { get; set; }
                public String CREDIT_RPT { get; set; }
                public String SIGIN_FLG { get; set; }
            }
        }

        // POST api/<controller>
        public UspAoAppUpdateDTO Post(UspAoAppUpdateDTI uspAoAppUpdateDTI)
        {
            return get(uspAoAppUpdateDTI);
        }


        private UspAoAppUpdateDTO get(UspAoAppUpdateDTI uspAoAppUpdateDTI)
        {
            UspAoAppUpdateDTO uspAoAppUpdateDTO = new UspAoAppUpdateDTO();

            try
            {
                string connectionString = WebConfigurationManager.AppSettings["MSSQLConnection"].ToString();
                SqlConnection conn = new SqlConnection(connectionString);
                SqlCommand cmd = new SqlCommand(string.Empty, conn);
                conn.Open();

                cmd.Parameters.Clear();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "USP_AO_APP_UPDATE";

                cmd.Parameters.Add("@IN_MOBILE_NO", SqlDbType.VarChar).Value = uspAoAppUpdateDTI.MOBILE_NO;
                cmd.Parameters.Add("@IN_MOBILE_ID", SqlDbType.VarChar).Value = uspAoAppUpdateDTI.MOBILE_ID;
                cmd.Parameters.Add("@IN_APP_CASE_NO_STR", SqlDbType.VarChar).Value = uspAoAppUpdateDTI.APP_CASE_NO;

                SqlDataReader reader = cmd.ExecuteReader(); //CommandBehavior.SingleRow

                if (reader.HasRows)
                {

                    DataTable dt = new DataTable();
                    dt.Load(reader);

                    var list = (from x in dt.AsEnumerable()
                                select x).ToList();

                    foreach (var item in list)
                    {
                        var d = new UspAoAppUpdateDTO.Data();
                        d.LOGIN_RESULT = item["LOGIN_RESULT"].ToString();
                        d.ERR_MSG = item["ERR_MSG"].ToString();
                        d.ERR_NO = item["ERR_NO"].ToString();
                        d.APP_CASE_NO = item["APP_CASE_NO"].ToString();
                        d.APPL_NO = item["APPL_NO"].ToString();
                        d.P_ID = item["P_ID"].ToString();
                        d.CH_NAME = item["CH_NAME"].ToString();
                        d.CASE_STATUS = item["CASE_STATUS"].ToString();
                        d.UPDATE_FLG = item["UPDATE_FLG"].ToString();
                        d.PROD_TYPE = item["PROD_TYPE"].ToString();
                        d.PROD_TYPE_NM = item["PROD_TYPE_NM"].ToString();
                        d.FUND_SRC = item["FUND_SRC"].ToString();
                        d.FUND_SRC_NM = item["FUND_SRC_NM"].ToString();
                        d.BANK_NA = item["BANK_NA"].ToString();
                        d.BANK_NA_NM = item["BANK_NA_NM"].ToString();
                        d.PROJ_ID = item["PROJ_ID"].ToString();
                        d.PROJ_ID_NM = item["PROJ_ID_NM"].ToString();
                        d.DLR_NO = item["DLR_NO"].ToString();
                        d.DLR_NO_NM = item["DLR_NO_NM"].ToString();
                        d.BRNH_NO = item["BRNH_NO"].ToString();
                        d.BRNH_NO_NM = item["BRNH_NO_NM"].ToString();
                        d.SALES_NO = item["SALES_NO"].ToString();
                        d.SALES_NO_NM = item["SALES_NO_NM"].ToString();
                        d.MSG_CNT = item["MSG_CNT"].ToString();
                        d.SEQ_NO = item["SEQ_NO"].ToString();
                        d.BACK_MENT = item["BACK_MENT"].ToString();
                        d.CREDIT_RPT = item["CREDIT_RPT"].ToString();
                        d.SIGIN_FLG = item["SIGIN_FLG"].ToString();
                        uspAoAppUpdateDTO.data.Add(d);
                    }
                }

                reader.Close();
                reader.Dispose();

                conn.Close();
                conn.Dispose();
                //---------------------------
                //      Message
                //---------------------------
                uspAoAppUpdateDTO.ERROR_CODE = "200";
                uspAoAppUpdateDTO.ERROR_MSG = "成功";

            }
            catch (Exception ex)
            {
                uspAoAppUpdateDTO.ERROR_CODE = "001";
                uspAoAppUpdateDTO.ERROR_MSG = "發生例外: " + ex.ToString();
                //Console.WriteLine("Failed to connect to database due to" + ex.ToString());
            }



            return uspAoAppUpdateDTO;
        }

    }
}