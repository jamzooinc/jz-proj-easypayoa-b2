﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Configuration;
using System.Web.Http;

namespace hfceasypay_WBYHFC12.WebApiControllers
{

    //*********************************************************************
    //*********************************************************************
    //      ip
    //=====================================================================
    //http://localhost:59025/api/USP_AO_APP_MSG_FIN


    //*********************************************************************
    //*********************************************************************
    //      DTI(data to in)   and   DTO(data to out)   class
    //=====================================================================
    public class USP_AO_APP_MSG_FIN_DTI
    {
        public String MOBILE_NO { get; set; }
        public String MOBILE_ID { get; set; }
        public String APP_CASE_NO { get; set; }

    }

    public class USP_AO_APP_MSG_FIN_DTO
    {
        public USP_AO_APP_MSG_FIN_DTO()
        {
            data = new List<Data>();
        }

        public String ERROR_CODE { get; set; }
        public String ERROR_MSG { get; set; }
        public List<Data> data;

        public class Data
        {
            public String LOGIN_RESULT { get; set; }
            public String ERR_MSG { get; set; }
            public String APP_CASE_NO { get; set; }
            public String MSG_NO { get; set; }
            public String MSG_CONTENT { get; set; }
            public String MSG_DATE { get; set; }
        }



    }



    //*********************************************************************
    //*********************************************************************
    //
    //=====================================================================
    public class USP_AO_APP_MSG_FINController : ApiController
    {


        // POST api/<controller>
        public USP_AO_APP_MSG_FIN_DTO Post(USP_AO_APP_MSG_FIN_DTI USP_AO_APP_MSG_FIN_DTI)
        {
            return USP_AO_APP_MSG_FIN(USP_AO_APP_MSG_FIN_DTI);
        }

        private USP_AO_APP_MSG_FIN_DTO USP_AO_APP_MSG_FIN(USP_AO_APP_MSG_FIN_DTI USP_AO_APP_MSG_FIN_DTI)
        {
            USP_AO_APP_MSG_FIN_DTO USP_AO_APP_MSG_FIN_DTO = new USP_AO_APP_MSG_FIN_DTO();

            try
            {
                string connectionString = WebConfigurationManager.AppSettings["MSSQLConnection"].ToString();
                SqlConnection conn = new SqlConnection(connectionString);
                SqlCommand cmd = new SqlCommand(string.Empty, conn);
                conn.Open();

                cmd.Parameters.Clear();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "USP_AO_APP_MSG_FIN";

                cmd.Parameters.Add("@IN_MOBILE_NO", SqlDbType.VarChar).Value = USP_AO_APP_MSG_FIN_DTI.MOBILE_NO;
                cmd.Parameters.Add("@IN_MOBILE_ID", SqlDbType.VarChar).Value = USP_AO_APP_MSG_FIN_DTI.MOBILE_ID;
                cmd.Parameters.Add("@IN_APP_NO", SqlDbType.VarChar).Value = USP_AO_APP_MSG_FIN_DTI.APP_CASE_NO;

                SqlDataReader reader = cmd.ExecuteReader(); //CommandBehavior.SingleRow

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        USP_AO_APP_MSG_FIN_DTO.Data data = new USP_AO_APP_MSG_FIN_DTO.Data();
                        data.LOGIN_RESULT = reader["LOGIN_RESULT"].ToString();
                        data.ERR_MSG = reader["ERR_MSG"].ToString();
                        data.APP_CASE_NO = reader["APPL_NO"].ToString();
                        data.MSG_NO = reader["MSG_NO"].ToString();
                        data.MSG_CONTENT = reader["MSG_CONTENT"].ToString();
                        data.MSG_DATE = reader["MSG_DATE"].ToString();
                        USP_AO_APP_MSG_FIN_DTO.data.Add(data);

                    }
                }

                reader.Close();
                reader.Dispose();

                conn.Close();
                conn.Dispose();
                //---------------------------
                //      Message
                //---------------------------
                USP_AO_APP_MSG_FIN_DTO.ERROR_CODE = ErrorInfo.OK_CODE;
                USP_AO_APP_MSG_FIN_DTO.ERROR_MSG = ErrorInfo.OK_MSG;

            }
            catch (Exception ex)
            {
                USP_AO_APP_MSG_FIN_DTO.ERROR_CODE = ErrorInfo.CATCH_CODE;
                USP_AO_APP_MSG_FIN_DTO.ERROR_MSG = ErrorInfo.CATCH_MSG + ex.ToString();
            }



            return USP_AO_APP_MSG_FIN_DTO;
        }
    }
}