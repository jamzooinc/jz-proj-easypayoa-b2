﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Configuration;
using System.Web.Http;

namespace hfceasypay_WBYHFC12.WebApiControllers
{


    //*********************************************************************
    //*********************************************************************
    //      ip
    //=====================================================================
    //http://localhost:59025/api/USP_AO_CASE_QRY


    //*********************************************************************
    //*********************************************************************
    //      DTI(data to in)   and   DTO(data to out)   class
    //=====================================================================
    public class USP_AO_CASE_QRY_DTI
    {
        public String MOBILE_NO { get; set; }
        public String MOBILE_ID { get; set; }
        public String APP_CASE_NO { get; set; }
        public String APPL_NO { get; set; }

    }

    public class USP_AO_CASE_QRY_DTO
    {
        public USP_AO_CASE_QRY_DTO()
        {
            data = new Data();
        }

        public String ERROR_CODE { get; set; }
        public String ERROR_MSG { get; set; }
        public Data data;

        public class Data
        {
            public String LOGIN_RESULT { get; set; }
            public String ERR_MSG { get; set; }
            public String APP_CASE_NO { get; set; }
            public String ENTER_DATE { get; set; }
            public String APPL_NO { get; set; }
            public String CH_NAME { get; set; }
            public String CASE_STATUS { get; set; }
            public String PROD_TYPE { get; set; }
            public String PROJ_NAME { get; set; }
            public String CAR_INFO { get; set; }
            public String PAY_BAL { get; set; }
            public String TERM_AMT { get; set; }
            public String TERM_INT { get; set; }
            public String F_AUDIT_RESULT { get; set; }
            public String R_AUDIT_RESULT { get; set; }
            public String EXPLAIN { get; set; }
            public String CHECK_MEMO { get; set; }

        }
    }



    //*********************************************************************
    //*********************************************************************
    //
    //=====================================================================

    public class USP_AO_CASE_QRYController : ApiController
    {


        // POST api/<controller>
        public USP_AO_CASE_QRY_DTO Post(USP_AO_CASE_QRY_DTI usp_AO_CASE_QRY_DTI)
        {
            return USP_AO_CASE_QRY(usp_AO_CASE_QRY_DTI);
        }

        private USP_AO_CASE_QRY_DTO USP_AO_CASE_QRY(USP_AO_CASE_QRY_DTI usp_AO_CASE_QRY_DTI)
        {
            USP_AO_CASE_QRY_DTO usp_AO_CASE_QRY_DTO = new USP_AO_CASE_QRY_DTO();

            try
            {
                string connectionString = WebConfigurationManager.AppSettings["MSSQLConnection"].ToString();
                SqlConnection conn = new SqlConnection(connectionString);
                SqlCommand cmd = new SqlCommand(string.Empty, conn);
                conn.Open();

                cmd.Parameters.Clear();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "USP_AO_CASE_QRY";

                cmd.Parameters.Add("@IN_MOBILE_NO", SqlDbType.VarChar).Value = usp_AO_CASE_QRY_DTI.MOBILE_NO;
                cmd.Parameters.Add("@IN_MOBILE_ID", SqlDbType.VarChar).Value = usp_AO_CASE_QRY_DTI.MOBILE_ID;
                cmd.Parameters.Add("@IN_APP_CASE_NO", SqlDbType.VarChar).Value = usp_AO_CASE_QRY_DTI.APP_CASE_NO;
                cmd.Parameters.Add("@IN_APPL_NO", SqlDbType.VarChar).Value = usp_AO_CASE_QRY_DTI.APPL_NO;

                

                SqlDataReader reader = cmd.ExecuteReader(); //CommandBehavior.SingleRow

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        usp_AO_CASE_QRY_DTO.data.LOGIN_RESULT = reader["LOGIN_RESULT"].ToString();
                        usp_AO_CASE_QRY_DTO.data.ERR_MSG = reader["ERR_MSG"].ToString();
                        usp_AO_CASE_QRY_DTO.data.APP_CASE_NO = reader["APP_CASE_NO"].ToString();
                        usp_AO_CASE_QRY_DTO.data.ENTER_DATE = reader["ENTER_DATE"].ToString();
                        usp_AO_CASE_QRY_DTO.data.APPL_NO = reader["APPL_NO"].ToString();
                        usp_AO_CASE_QRY_DTO.data.CH_NAME = reader["CH_NAME"].ToString();
                        usp_AO_CASE_QRY_DTO.data.CASE_STATUS = reader["CASE_STATUS"].ToString();
                        usp_AO_CASE_QRY_DTO.data.PROD_TYPE = reader["PROD_TYPE"].ToString();
                        usp_AO_CASE_QRY_DTO.data.PROJ_NAME = reader["PROJ_NAME"].ToString();
                        usp_AO_CASE_QRY_DTO.data.CAR_INFO = reader["CAR_INFO"].ToString();
                        usp_AO_CASE_QRY_DTO.data.PAY_BAL = reader["PAY_BAL"].ToString();
                        usp_AO_CASE_QRY_DTO.data.TERM_AMT = reader["TERM_AMT"].ToString();
                        usp_AO_CASE_QRY_DTO.data.TERM_INT = reader["TERM_INT"].ToString();
                        usp_AO_CASE_QRY_DTO.data.F_AUDIT_RESULT = reader["F_AUDIT_RESULT"].ToString();
                        usp_AO_CASE_QRY_DTO.data.R_AUDIT_RESULT = reader["R_AUDIT_RESULT"].ToString();
                        usp_AO_CASE_QRY_DTO.data.EXPLAIN = reader["EXPLAIN"].ToString();
                        usp_AO_CASE_QRY_DTO.data.CHECK_MEMO = reader["CHECK_MEMO"].ToString();
                    }
                }

                reader.Close();
                reader.Dispose();

                conn.Close();
                conn.Dispose();
                //---------------------------
                //      Message
                //---------------------------
                usp_AO_CASE_QRY_DTO.ERROR_CODE = ErrorInfo.OK_CODE;
                usp_AO_CASE_QRY_DTO.ERROR_MSG = ErrorInfo.OK_MSG;

            }
            catch (Exception ex)
            {
                usp_AO_CASE_QRY_DTO.ERROR_CODE = ErrorInfo.CATCH_CODE;
                usp_AO_CASE_QRY_DTO.ERROR_MSG = ErrorInfo.CATCH_MSG + ex.ToString();
            }



            return usp_AO_CASE_QRY_DTO;
        }

    }
}