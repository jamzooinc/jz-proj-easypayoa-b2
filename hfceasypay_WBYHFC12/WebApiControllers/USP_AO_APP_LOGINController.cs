﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data;
using System.Data.SqlClient;

using System.Web;
using System.Web.Configuration;

namespace hfceasypay_WBYHFC12.WebApiControllers
{

    //*********************************************************************
    //*********************************************************************
    //      ip
    //=====================================================================
    //http://localhost:59025/api/USP_AO_APP_LOGIN


    //*********************************************************************
    //*********************************************************************
    //      DTI(data to in)   and   DTO(data to out)   class
    //=====================================================================
    public class LoginDTI
    {
        public String SALES_NO { get; set; }
        public String MOBILE_NO { get; set; }
        public String MOBILE_ID { get; set; }
        public String MOBILE_TYPE { get; set; }
        public String OS_NAME { get; set; }
        public String OS_VERNO { get; set; }
    }

    public class LoginDTO
    {
        public LoginDTO(){
            data = new Data();
        }

        public String ERROR_CODE { get; set; }
        public String ERROR_MSG { get; set; }
        public Data data;

        public class Data
        {
            public String LOGIN_RESULT { get; set; }
            public String ERR_NO { get; set; }
            public String ERR_MSG { get; set; }
            public String FTP_IP { get; set; }
            public String FTP_PORT { get; set; }
            public String FTP_ACCT { get; set; }
            public String FTP_PWD { get; set; }
            public String PARAM_TIMER { get; set; }
            public String TAG_NO { get; set; }
            public String POST_URL { get; set; }
            public String BD_Timer { get; set; }
            public String DOWNLOAD_FLG { get; set; }
            public String MEM_LIMIT { get; set; }
        }



    }



    //*********************************************************************
    //*********************************************************************
    //
    //=====================================================================
    public class USP_AO_APP_LOGINController : ApiController
    {

        // GET api/<controller>/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<controller>
        public LoginDTO Post(LoginDTI loginDTI)
        {
            return get(loginDTI);
        }


        private LoginDTO get(LoginDTI loginDTI)
        {
            LoginDTO loginDTO = new LoginDTO();

            try
            {
                string connectionString = WebConfigurationManager.AppSettings["MSSQLConnection"].ToString();
                SqlConnection conn = new SqlConnection(connectionString);
                SqlCommand cmd = new SqlCommand(string.Empty, conn);
                conn.Open();

                cmd.Parameters.Clear();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "USP_AO_APP_LOGIN";

                cmd.Parameters.Add("@IN_SALES_NO", SqlDbType.VarChar).Value = loginDTI.SALES_NO;           //"????";
                cmd.Parameters.Add("@IN_MOBILE_NO", SqlDbType.VarChar).Value = loginDTI.MOBILE_NO;         //"0937121235";
                cmd.Parameters.Add("@IN_MOBILE_ID", SqlDbType.VarChar).Value = loginDTI.MOBILE_ID;         //"edfvghyt123";
                cmd.Parameters.Add("@IN_MOBILE_TYPE", SqlDbType.VarChar).Value = loginDTI.MOBILE_TYPE;     //"ausu";
                cmd.Parameters.Add("@IN_OS_NAME", SqlDbType.VarChar).Value = loginDTI.OS_NAME;             //"Android";
                cmd.Parameters.Add("@IN_OS_VERNO", SqlDbType.VarChar).Value = loginDTI.OS_VERNO;           //"7";

                SqlDataReader reader = cmd.ExecuteReader(); //CommandBehavior.SingleRow

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        loginDTO.data.LOGIN_RESULT = reader["LOGIN_RESULT"].ToString();
                        loginDTO.data.ERR_NO = reader["ERR_NO"].ToString();
                        loginDTO.data.ERR_MSG = reader["ERR_MSG"].ToString();
                        loginDTO.data.FTP_IP = reader["FTP_IP"].ToString();
                        loginDTO.data.FTP_PORT = reader["FTP_PORT"].ToString();
                        loginDTO.data.FTP_ACCT = reader["FTP_ACCT"].ToString();
                        loginDTO.data.FTP_PWD = reader["FTP_PWD"].ToString();
                        loginDTO.data.PARAM_TIMER = reader["PARAM_TIMER"].ToString();
                        loginDTO.data.TAG_NO = reader["TAG_NO"].ToString();
                        loginDTO.data.POST_URL = reader["POST_URL"].ToString();
                        loginDTO.data.BD_Timer = reader["BD_Timer"].ToString();
                        loginDTO.data.DOWNLOAD_FLG = reader["DOWNLOAD_FLG"].ToString();
                        loginDTO.data.MEM_LIMIT = reader["MEM_LIMIT"].ToString();
                    }
                }

                reader.Close();
                reader.Dispose();

                conn.Close();
                conn.Dispose();
                //---------------------------
                //      Message
                //---------------------------
                loginDTO.ERROR_CODE = "200";
                loginDTO.ERROR_MSG = "成功";

            }
            catch (Exception ex)
            {
                loginDTO.ERROR_CODE = "001";
                loginDTO.ERROR_MSG = "發生例外: " + ex.ToString();
                //Console.WriteLine("Failed to connect to database due to" + ex.ToString());
            }



            return loginDTO;
        }


    }
}